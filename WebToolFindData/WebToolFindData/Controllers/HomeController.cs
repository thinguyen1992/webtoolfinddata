﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using WebToolFindData.Entity;
using Newtonsoft.Json.Linq;
using System.Net;

namespace WebToolFindData.Controllers
{
    public class HomeController : Controller
    {
        private string finalString = "";
        public ActionResult Index()
        {
            return View();
        }
        private Section[] GetSectionArray(string resultSection)
        {
            Section[] arraySection;
            arraySection = JObject.Parse(resultSection)["collections"].ToObject<Section[]>();
            return arraySection;

        }
        private Article[] GetArticleArray(string resultArticle)
        {
            Article[] arrayArticle;
            arrayArticle = JObject.Parse(resultArticle)["contents"].ToObject<Article[]>();
            return arrayArticle;
        }

        [HttpPost]
        public ActionResult Index(string environment, string editionDate, string datatype,string section){
            List<DataFromJson> listResult = new List<DataFromJson>();
            string host;

            using (WebClient wc = new WebClient())
            {
                string url;
                string sectionID ="";
                if(environment == "SIT"){
                    host = "https://www.theaustraliansit.com.au/app-feed/";
                }
                else{
                    host = "https://app.theaustralian.com.au/app-feed/";
                }

                url = host + "edition/" + editionDate + ".json";
                var json = wc.DownloadString(url);
                Section[] arraySection = GetSectionArray(json);
                foreach (Section sectionInArray in arraySection ){
                    if(sectionInArray.name == section){
                        sectionID = sectionInArray.id;
                        break;
                    }
                }

                var jsonSection = wc.DownloadString(host + "section/" + sectionID + ".json");
                Article[] arrayArticle = GetArticleArray(jsonSection);
                foreach (Article article in arrayArticle)
                {   
                    DataFromJson data = new DataFromJson();
                    data.sectionName = section;
                    var jsonArticle = wc.DownloadString(host + "article/" + article.id + ".json");
                    if (jsonArticle.Contains(datatype))
                    {
                          data.articleName = article.name;
                          listResult.Add(data);
                    }
                 }
            }

            foreach (DataFromJson resultString in listResult)
            {
                finalString = finalString + "Section Name:" + resultString.sectionName + "\n" + "Article Name:" + resultString.articleName + "\n";
            }

            ViewData["FinalString"] = finalString;

            return View();

            
        }


    }
}
