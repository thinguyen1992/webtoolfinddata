﻿using Newtonsoft.Json;
using System;
namespace WebToolFindData.Entity
{
    public class Article
    {
            [JsonProperty("id")]
            public string id { get; set; }
            [JsonProperty("title")]
            public string name { get; set; }
    }
}
