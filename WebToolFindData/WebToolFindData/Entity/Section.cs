﻿using System;
using Newtonsoft.Json;

namespace WebToolFindData.Entity
{
    public class Section
    {
            [JsonProperty("id")]
            public string id { get; set; }
            [JsonProperty("name")]
            public string name { get; set; }
    }
}
