﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebToolFindData.Models
{
    public class Data
    {
        public string EditionDate { get; set; }
        public string DataType { get; set; }
        [Display(Name = "EnvironmentDetail")]
        public List<string> ListEnvironment{ get; set; }

        [Display(Name = "selectedEnvironment")]
        public string SelectedEnvironment { get; set; }
    }
}
